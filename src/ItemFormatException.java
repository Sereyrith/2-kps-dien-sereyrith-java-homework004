
public class ItemFormatException extends NullPointerException {
    ItemFormatException(){
        super();
    }
    ItemFormatException(String message){
        super(message);
    }
}
