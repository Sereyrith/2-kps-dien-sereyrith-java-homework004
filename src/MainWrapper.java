public class MainWrapper {
    public static void main(String[] args) {
        Wrapper<Integer> integerWrapper = new Wrapper<>();
        try {
//            integerWrapper.addItem(1);
//            integerWrapper.addItem(2);
//            integerWrapper.addItem(2);
//            integerWrapper.addItem(null);
            for (int i=1;i<30;i++){
                integerWrapper.addItem(i);
            }
        } catch (ItemFormatException e) {
            System.out.println(e);
        }

        System.out.println("List all of inputted");
        for (int i=0;i<integerWrapper.size(); i++){
            System.out.println(integerWrapper.getItem(i));
        }
    }
}
