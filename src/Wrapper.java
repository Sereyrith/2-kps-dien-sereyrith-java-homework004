import java.util.ArrayList;
import java.util.List;

public class Wrapper<E>{
    private  List<E> list = new ArrayList<>();

    public void addItem(E i) {
        for (int k=0;k<list.size();k++){
            if (getItem(k)==i){
                throw new ItemFormatException("Duplication value : "+i);
            }else if(null==i){
                throw new ItemFormatException("Inputted null value");
            }
        }
            list.add(i);
    }

    public int size(){
        return list.size();
    }

    public E getItem(int i){
        return list.get(i);
    }
}
